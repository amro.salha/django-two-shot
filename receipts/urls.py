from django.urls import path
from .views import get_receipts, create_receipt, create_expense_category, create_account, category_list, account_list

urlpatterns = [
    path("", get_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/create/", create_expense_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
]
