from django.shortcuts import render
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.shortcuts import redirect

# Create your views here.

@login_required
def get_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts" : receipts
    }
    return render(request, "receipts/list.html", context)

@login_required
def category_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expenses" : expenses
    }
    return render(request, "receipts/category_list.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts" : accounts
    }
    return render(request, "receipts/account_list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form" : form
    }
    return render(request, "receipts/create.html", context)

@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
    "form" : form
    }
    return render(request, "receipts/create_expense_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
    "form" : form
    }
    return render(request, "receipts/create_account.html", context)
